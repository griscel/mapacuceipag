<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>MapaCUCEI</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="css/marcadores.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    </head>

@include('sidebar')

        <div id="content">
            
            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                <i class="glyphicon glyphicon-align-left"></i>
                <span>Menú</span>
            </button>
            <br>
            <br>
            <div class="tabla">
                <div class="titulos">
                    <div class="elemento2"><h4>Fecha</h4></div>
                    <div class="elemento2"><h4>Asunto</h4></div>
                    <div class="elemento2"><h4>Abrir</h4></div>
                </div>
            </div>
            <br>

            <div class="tabla">       
                <div class="titulos">
                    <div class="elemento2"><h4>01/02/19</h4></div>
                    <div class="elemento2"><h4>Error</h4></div>
                    <div class="elemento2">
                        <button onclick="location.href='/mensaje';" type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <span>&#x2709;  Abrir</span>
                        </button>
                    </div>
                </div>
            </div>
       </div>
    </body>
</html>