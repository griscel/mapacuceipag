
    <body>

        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h3>Mapa CUCEI</h3>
                </div>

                <ul class="list-unstyled components">
                    <p>Nombre</p>
                    <li >
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Marcadores</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li><a href="/marcadores">Todos</a></li>
                            <li><a href="#">Edificios</a></li>
                            <li><a href="#">Servicios</a></li>
                            <li><a href="#">Cafeterías</a></li>
                            <li><a href="#">Papelerías</a></li>
                            <li><a href="#">Auditorios</a></li>
                            <li><a href="#">&#x271A; Añadir</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/mensajes">Mensajería</a>
                        <a href="/configuracion">Configuración</a>
                    </li>
                    <li>
                        <a href="#">Salir</a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content, #instruccion').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
   