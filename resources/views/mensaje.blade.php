<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>MapaCUCEI</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="css/mensaje.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    </head>

@include('sidebar')

        <div id="content">
            
            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                <i class="glyphicon glyphicon-align-left"></i>
                <span>Menú</span>
            </button>
            <br>
            <br>
            <div class="mensaje">
                <div class="encabezado">
                    <div class="titulo"><h3>Mensaje</h3></div>
                    <div class="responder">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <span>&#x2710;  Responder</span>
                        </button>
                    </div>
                </div>
            </div>            
            <div class="contenido"><h5>Descripción:</h5> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
            <div class="contenido">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </div>
       </div>
    </body>
</html>