<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>MapaCUCEI</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="css/formconfig.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    </head>
    
@include('sidebar')

        <div id="content">
            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                <i class="glyphicon glyphicon-align-left"></i>
                <span>Menú</span>
            </button>
            <div class="container">
                <div class="form__top">
                    <h3>Configuración</h3>
                </div>		
                <form class="form__reg" action="">
                    <input class="input" type="text" value="Nombre" required autofocus>
                    <input class="input" type="email" value="Email" required>
                    <input class="input" type="password" placeholder="Contraseña" required>
                    <input class="input" type="password" placeholder="Repetir Contraseña" required>
                    <div class="btn__form">
                        <input class="btn__submit" type="submit" value="Cambiar">
                    </div>
                </form>
            </div>
            
        </div>
        
    </body>
</html>