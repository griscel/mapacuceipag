<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>MapaCUCEI</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="css/marcadores.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    </head>

@include('sidebar')

        <div id="content">
            
            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                <i class="glyphicon glyphicon-align-left"></i>
                <span>Menú</span>
            </button>
            <input type="text" placeholder="Buscar" class="busqueda">
            <button type="button" id="sidebarCollapse" class="busqueda btn btn-info navbar-btn">
            <span>Buscar</span>
            </button>
            <br>
            <br>
            <div class="tabla">
                <div class="titulos">
                    <div class="elemento"><h4>Nombre</h4></div>
                    <div class="elemento"><h4>Editar</h4></div>
                    <div class="elemento"><h4>Eliminar</h4></div>
                </div>
            </div>
            <br>

            <div class="tabla">       
                <div class="titulos">
                    <div class="elemento"><h4>X</h4></div>
                    <div class="elemento"><button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <span>&#x270E;  Editar</span>
                    </button></div>
                    <div class="elemento"><button type="button" id="sidebarCollapse" class="btn btn:hover navbar-btn boton">
                        <span>&#x2716;  Eliminar</span>
                    </button></div>
                </div>
            </div>

       </div>
    </body>
</html>