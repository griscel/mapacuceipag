<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/login','LoginController@index');

Route::get('/inicio','InicioController@index');

Route::get('/marcadores','MarkersController@index');

Route::get('/configuracion','ConfigController@index');

Route::get('/mensajes','MessagesController@index');

Route::get('/mensaje','MessageController@index');


